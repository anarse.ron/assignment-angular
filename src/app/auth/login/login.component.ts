import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLoading = false;
  get f() { return this.loginForm.controls; }
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl;
  error = null;



  constructor(private fb: FormBuilder, private http: HttpClient, private auth: AuthService, private route: ActivatedRoute, private router: Router) { }


  ngOnInit() {

    
    this.loginForm = new FormGroup({
      'userData': new FormGroup({
        email: new FormControl('', [Validators.required,
          Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")]),
        password: new FormControl('', [Validators.required,])
      })
    });


        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['user-list'] || '/';

  }


  onSubmit() {
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    else{
      this.auth.login(this.loginForm.value.userData);

      console.log(this.loginForm);

    }
 

  }
}