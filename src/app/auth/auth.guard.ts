import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AuthService } from "./auth.service";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    if (localStorage.getItem('token')) {
      // logged in so return true
      return true;
  }

  // not logged in so redirect to login page with the return url and return false
  this.router.navigate(['login'], { queryParams: { returnUrl: state.url }});
  alert('not authorised to view please login')

  return false;
}
  
  
}

