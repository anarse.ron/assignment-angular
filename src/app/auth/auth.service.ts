import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Subject, Observable, of, throwError } from "rxjs";
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthData } from './auth-data.model';

@Injectable({ providedIn: "root" })
export class AuthService {
  private isAuthenticated = false;
  private token: string;
  private tokenTimer: any;
  private authStatusListener = new Subject<boolean>();

  baseUrl = environment.baseUrl;

  error = new Subject<string>();

  constructor(private http: HttpClient, private router: Router) { }

  getToken() {
    return this.token;
  }

  getIsAuth() {
    return this.isAuthenticated;
  }


  login(authData : AuthData) {

    this.http
      .post<{ token: string }>(this.baseUrl + 'login', authData)
      .subscribe(response => {
        console.log(response)
        const token = response.token;
        console.log(token)
        this.token = token;
        if (token) {
          this.isAuthenticated = true;
          this.saveAuthData(token);
          this.router.navigateByUrl('/user-list');
        }

      },
        error => {
          this.error.next(error.message);
          console.log(this.error)
        }

      );
  }



  private saveAuthData(token: string) {
    localStorage.setItem("token", token);
  }
}
